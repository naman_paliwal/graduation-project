1. Why Messaging Queue exists?
Ans. Messaging queues implement an asynchronous communication pattern between two or more processes/threads whereas the sending and receiving party do not need to interact with the message queue at the same time. Messages placed onto the queue are stored until the recipient retrieves them. Message queues have implicit or explicit limits on the size of data that may be transmitted in a single message and the number of messages that may remain outstanding on the queue.

2.What this system is all about?
Ans. This system is a client to the client communication model wherein three different autonomous sub-component which is client-server-client.

3. How a system function?
Ans. A systen consists of a group of entities which interact with each other. It is surrounded and influenced by its environment. A system might consist of its subsystem. A subsystem is a set of elements which has the ability to interact with local and remote operators.

4.Which problem(s) do you see that you’ll be going to address?
Ans. Here in this system, we are going to discuss asynchronous communication among various clients.

5.What are the different use cases where you can see this system can use as a solution?
Ans. This system can be used to achieve the following objective
-> Understanding Data Flow
-> Buffering
-> Asynchronous Communication

6.What are the different solutions that are available in the market?
Ans. -> RabbitMQ
->Apache Kafka
->AWS MQ
->Apache ActiveMQ