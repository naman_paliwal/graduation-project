﻿using IMQServer.MessagingQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IMQServer.Tests.BusinessLogic
{
    public class QueueTests
    {
        [Fact]
        void EnqueueAndDequeueTest()
        {
            string message1 = "this is message1", message2 = "this is message2", message3 = "this is message3";
            MQueue mQueue = new MQueue(new Topic() { Id = 1, Name = "Test"});
            mQueue.Enqueue(new Message() { ClientMessage = "this is message1", date = DateTime.Now.AddHours(-4) });
            mQueue.Enqueue(new Message() { ClientMessage = "this is message2", date = DateTime.Now.AddHours(-3) });
            mQueue.Enqueue(new Message() { ClientMessage = "this is message3", date = DateTime.Now.AddHours(-2) });

            Assert.Equal(mQueue.front.message.ClientMessage, message1);
            Assert.Equal(mQueue.rear.message.ClientMessage, message3);

            mQueue.Dequeue();

            Assert.Equal(mQueue.front.message.ClientMessage, message2);
            Assert.Equal(mQueue.rear.message.ClientMessage, message3);
        }

        [Fact]
        void UnreadMessageTest()
        {
            string message1 = "this is message1", message2 = "this is message2", message3 = "this is message3";
            MQueue mQueue = new MQueue(new Topic() { Id = 1, Name = "Test" });

            mQueue.Enqueue(new Message() { ClientMessage = "this is message3", date = DateTime.Now.AddHours(-4) });
            mQueue.Enqueue(new Message() { ClientMessage = "this is message1", date = DateTime.Now.AddHours(2) });
            mQueue.Enqueue(new Message() { ClientMessage = "this is message2", date = DateTime.Now.AddHours(3) });


            string serializedMessages = mQueue.GetUnreadMessage(DateTime.Now);

            Assert.Contains(message1, serializedMessages);
            Assert.Contains(message2, serializedMessages);
            Assert.DoesNotContain(message3, serializedMessages);

        }
    }
}
