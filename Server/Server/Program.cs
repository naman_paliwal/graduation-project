﻿using IMQServer.IMQ.DAL;
using IMQServer.IMQ.DAL.Repository;
using IMQServer.MessagingQueue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace IMQServer
{
    class Program
    {
        static void Main(string[] args)
        {   
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));

            log.Info("Server has started");
            String clientName;
            byte[] bytes = new Byte[200];
            ServerService serverService = new ServerService();
            serverService.StartServer();
            IMQEntities context = new IMQEntities();
            List<MQueue> queueList = new List<MQueue>();
            IGenericRepository<User> userRepo = new GenericRepository<User>(context);
            IGenericRepository<Subscriber> subscriberRepo = new GenericRepository<Subscriber>(context);
            IGenericRepository<Topic> topicRepo = new GenericRepository<Topic>(context);
            List<Topic> topicList = topicRepo.Get(x => true).ToList();
            topicList.ForEach(x => queueList.Add(new MQueue(x)));
            CryptoEngine cryptoEngine = new CryptoEngine();
            IMQRequest imqRequest = new IMQRequest();
            IMQResponse imqResponse = new IMQResponse();
            List<QueueNode> deadlist = new List<QueueNode>(); 
            DeadLetter deadLetterQueue = new DeadLetter(queueList);
            var timer = new System.Threading.Timer(
            e => deadLetterQueue.timer1_Tick(queueList, deadlist),
            null,
            TimeSpan.Zero,
            TimeSpan.FromSeconds(Int32.Parse(ConfigurationManager.AppSettings.Get("MessageExpireTime"))));

            try
            {
                while (true)
                {
                    imqResponse = new IMQResponse();
                    Socket handler = serverService.listener.Accept();

                    HandleClient client = new HandleClient(context, ref queueList);
                    int bytesRec = handler.Receive(bytes);
                    string serializeMessage = cryptoEngine.DecryptString(Encoding.ASCII.GetString(bytes, 0, bytesRec).ToString());
                    imqRequest = JsonConvert.DeserializeObject<IMQRequest>(serializeMessage);

                    clientName = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    User user = userRepo.FindSingle(entity => entity.Id == imqRequest.userId && entity.Name == imqRequest.message);

                    if(imqRequest.userId == -1)
                    {
                        imqResponse.Status = "NewUser";
                        user = userRepo.Insert(new User() { Name = imqRequest.message });
                        userRepo.Save();
                    }
                    else if (user == null)
                    {
                        imqResponse.Status = "InvalidUser";
                        handler.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(imqResponse)));
                        continue;
                    }

                    if( user.Name == "root")
                    {
                        imqResponse.Status = "Admin";
                    }
                    imqResponse.Data = client.GetAllTopicsSerializeObject(user.Id);
                    imqResponse.userId = user.Id;

                    client.StartClient(handler, clientName, user);
                    handler.Send(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(imqResponse)));
                }
            }
            catch (Exception exception)
            {
                log.Error(exception);
                Console.WriteLine("System has ran into some technical issue. Please check the logs");
            }
            Console.ReadKey();
        }
    }
}