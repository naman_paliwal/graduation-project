﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.IMQ.DAL.Repository
{
    class GenericRepository<T>: IGenericRepository<T> where T:class
    {
        private IMQEntities context;
        private DbSet<T> entity;

        public GenericRepository(IMQEntities context)
        {
            this.context = context;
            entity = context.Set<T>();
        }

        public T Insert(T obj)
        {
            return this.entity.Add(obj);
        }

        public T FindSingle(Expression<Func<T, bool>> predicate)
        {
            var obj = this.entity.FirstOrDefault(predicate);
            return obj;
        }

        public void Save()
        {
            this.context.SaveChanges();
        }

        public IQueryable<T> Get(Expression<Func<T,bool>> pred)
        {
            return this.entity.Where(pred);
        }

        public List<Object> GetAllTopic(int userId)
        {
            List<object> list = new List<object>();
            var result = (from p in context.Topics
                       join d in context.Subscribers.Where(x => x.user_Id == userId) on p.Id equals d.topic_Id
                       into eGroup from d in eGroup.DefaultIfEmpty()
                       select new { TopicId = p.Id, TopicName = p.Name, UserId = d.user_Id, Role = d.Role.RoleName });
            foreach(var item in result)
            {
                if(item.UserId == userId || item.UserId == null)
                {
                    list.Add(item);
                }
            }            
            return list;
        }

        public DateTime GetAndUpdateLastLogin(int userId, int topicId)
        {
            Subscriber sub = this.context.Subscribers.First(x => x.user_Id == userId && x.topic_Id == topicId);
            DateTime? lastlogin = sub.lastlogintime;
            sub.lastlogintime = DateTime.Now;
            this.context.SaveChanges();
            return (DateTime)(lastlogin.HasValue ? lastlogin : DateTime.MinValue);
        }
    }
}
