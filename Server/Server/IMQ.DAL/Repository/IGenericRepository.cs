﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.IMQ.DAL.Repository
{
    public interface IGenericRepository<T> where T: class
    {
        T Insert(T obj);
        T FindSingle(Expression<Func<T, bool>> predicate);
        void Save();
        IQueryable<T> Get(Expression<Func<T, bool>> pred);
        List<Object> GetAllTopic(int userId);
        DateTime GetAndUpdateLastLogin(int userId, int topicId);
    }
}
