﻿using IMQServer.IMQ.DAL.Repository;
using IMQServer.MessagingQueue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using static IMQServer.Common;

namespace IMQServer
{
    class HandleClient
    {
        Socket client;
        public static string message = null;
        byte[] bytes = new Byte[Int64.Parse(ConfigurationManager.AppSettings.Get("maxMessageSize"))];
        ClientDataHandler dataHandler;
        CryptoEngine cryptoEngine;
        IMQRequest imqRequest;
        IMQResponse imqResponse;
        IMQEntities context;
        IGenericRepository<Message> messageRepo;
        IGenericRepository<Topic> topicRepo;
        IGenericRepository<Subscriber> subscriberRepo;
        User user;
        List<MQueue> queueList;

        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));


        public HandleClient (IMQEntities context, ref List<MQueue> queueList)
        {
            this.context = context;
            this.messageRepo = new GenericRepository<Message>(context);
            this.topicRepo = new GenericRepository<Topic>(context);
            this.subscriberRepo = new GenericRepository<Subscriber>(context);
            this.cryptoEngine = new CryptoEngine();
            this.imqRequest = new IMQRequest();
            this.imqResponse = new IMQResponse();
            this.queueList = queueList;
        }

        public void StartClient(Socket client, string clientName, User user)
        {
            Thread clientThread = new Thread(doChat);
            try
            {
                Console.WriteLine("Text received : ");
                this.client = client;
                this.dataHandler = new ClientDataHandler();
                clientThread.Start(user);
            }
            catch (Exception exception)
            {
                Console.WriteLine(clientName + " client is disconnected");
                log.Equals(exception);
                clientThread.Abort();
            }
        }

        void doChat(Object userObj)
        {
            this.user = (User)userObj;
            String clientName = user.Name;
            Topic topic = new Topic() { Id = 1 };
            int clientId = user.Id;
            Message messageObj = new Message() { User = user };
            try
            {
                while (true)
                {
                    int bytesRec = client.Receive(bytes);
                    string serializeMessage = this.cryptoEngine.DecryptString(Encoding.ASCII.GetString(bytes, 0, bytesRec).ToString());
                    this.imqRequest = JsonConvert.DeserializeObject<IMQRequest>(serializeMessage);
                    messageObj = new Message() { User = user };
                    if(this.imqRequest.action == UserAction.newchannel.ToString())
                    {
                        this.NewChannel();
                    }
                    else if (this.imqRequest.action == UserAction.broadcastmessage.ToString())
                    {
                        this.BroadcastMessage(messageObj);
                    }
                    else if (this.imqRequest.action == UserAction.subscribetochannel.ToString())
                    {
                        this.SubscribeToChannel();
                    }
                    else if (this.imqRequest.action == UserAction.readmessage.ToString())
                    {
                        this.ReadMessage();
                    }
                }  
            }
            catch (Exception exception)
            {
                log.Error(exception.InnerException.Message);
                return;
            }
        }

        public string GetAllTopicsSerializeObject(int userId)
        {
            var list = subscriberRepo.GetAllTopic(userId);
            // Open close principal
            string str = JsonConvert.SerializeObject(list, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            });
            return str;
        }

        void SendAcknowledgement()
        {
            string encryptString = this.cryptoEngine.EncryptString(JsonConvert.SerializeObject(this.imqResponse));
            byte[] msg = Encoding.ASCII.GetBytes(encryptString);
            client.Send(msg);
        }

        void NewChannel()
        {
            this.topicRepo.Insert(new Topic() { Name = imqRequest.info });
            this.topicRepo.Save();
            this.imqResponse = new IMQResponse();
            this.imqResponse.Data = this.GetAllTopicsSerializeObject(this.user.Id);
            this.imqResponse.Status = "success";
            this.SendAcknowledgement();
        }

        void BroadcastMessage(Message messageObj)
        {
            messageObj.ClientMessage = this.imqRequest.message;
            messageObj.user_Id = this.user.Id;
            messageObj.date = DateTime.Now;
            messageObj.topic_Id = this.imqRequest.channelId;
            Console.WriteLine("Message from " + this.user.Name + " : " + message);
            Console.WriteLine();
            this.dataHandler.SaveData(message, client);

            this.messageRepo.Insert(messageObj);
            this.messageRepo.Save();

            this.imqResponse.message = "Server has recived the message : '" + messageObj.ClientMessage + "'.";
            this.imqResponse.Status = "200";
            this.SendAcknowledgement();
            this.queueList.Find(x => x.topic.Id == messageObj.topic_Id).Enqueue(messageObj);
        }

        void SubscribeToChannel()
        {
            this.subscriberRepo.Insert(new Subscriber() { role_RoleId = Int32.Parse(ConfigurationManager.AppSettings.Get("subscriberRoleId")), 
                                                            topic_Id = this.imqRequest.channelId, user_Id = user.Id });
            this.subscriberRepo.Save();
            this.imqResponse = new IMQResponse();
            this.imqResponse.Status = "200";
            this.SendAcknowledgement();
        }

        void ReadMessage()
        {
            this.imqResponse = new IMQResponse();
            DateTime lastLogin = this.subscriberRepo.GetAndUpdateLastLogin(this.imqRequest.userId, this.imqRequest.channelId);
            this.imqResponse.Data = queueList.Find(x => x.topic.Id == this.imqRequest.channelId).GetUnreadMessage(lastLogin);
            Console.WriteLine();
            this.SendAcknowledgement();
        }
    }
}
