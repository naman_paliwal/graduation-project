﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer
{
    public class IMQProtocol
    {
        public IMQProtocol()
        {
            this.dataFormat = "JSON";
        }
        public string dataFormat;
        // Can use enum
        public string action;
        public int channelId;
        public string info;
        public string message;
        public int userId;
    }
}
