﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.MessagingQueue
{
    public class MQueue
    {
        public QueueNode front, rear;
        public Topic topic;

        public MQueue(Topic topic)
        {
            this.front = this.rear = null;
            this.topic = topic;
        }

        public void Enqueue(Message message)
        {
            QueueNode queueNode = new QueueNode(message);

            if (front == null && rear == null)
            {
                this.front = queueNode;
                this.rear = queueNode;
                return;
            }

            this.rear.next = queueNode;
            queueNode.previous = this.rear;
            this.rear = queueNode;
        }

        public void Dequeue()
        {
            if (front == null)
            {
                return;
            }

            if (front == rear)
            {
                this.front = this.rear = null;
                return;
            }

            this.front = this.front.next;
            this.front.previous = null;
        }

        public string GetUnreadMessage(DateTime lastSeenTime)
        {
            List<Message> unreadMessages = new List<Message>();
            QueueNode currentNode = this.rear;
            while(currentNode != null && currentNode.message.date > lastSeenTime)
            {
                unreadMessages.Add(currentNode.message);
                Console.Write(currentNode.message.ClientMessage + "     ");
                Console.WriteLine(currentNode.message.date);
                currentNode = currentNode.previous;
            }
            string str = JsonConvert.SerializeObject(unreadMessages, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                PreserveReferencesHandling = PreserveReferencesHandling.Objects
                            });
            Console.WriteLine(str);
            return str;
        }
    }
}
