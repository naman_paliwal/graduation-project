﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.MessagingQueue
{
    class DeadLetter
    {
        public DeadLetter(List<MQueue> queueList)
        {
        }

        public void timer1_Tick(List<MQueue> queueList, List<QueueNode> deadlist)
        {
            foreach(MQueue queue in queueList)
            {
                QueueNode currentNode = queue.front;
                while(currentNode != null && currentNode.message.date.AddSeconds(Int32.Parse(ConfigurationManager.AppSettings.Get("MessageExpireTime"))) < DateTime.Now )
                {
                    Console.WriteLine("this is deleted : " + queue.front.message.ClientMessage);
                    deadlist.Add(queue.front);
                    queue.Dequeue();
                    GC.Collect();
                    currentNode = currentNode.next;
                }
            }
        }
    }
}
