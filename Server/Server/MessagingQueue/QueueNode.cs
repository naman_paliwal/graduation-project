﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.MessagingQueue
{
    public class QueueNode
    {
        public Message message;
        public QueueNode next, previous;

        public QueueNode(Message message)
        {
            this.message = message;
            this.next = null;
            this.previous = null;
        }
    }
}
