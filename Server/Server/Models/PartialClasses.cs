﻿using System.ComponentModel.DataAnnotations;

namespace IMQServer.Models
{
    [MetadataType(typeof(RoleMetadata))]
    public partial class Role
    {

    }

    [MetadataType(typeof(TopicMetaData))]
    public partial class Topic
    {

    }

    [MetadataType(typeof(UserMetaData))]
    public partial class User
    {

    }
}
