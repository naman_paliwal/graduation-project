﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer.Models
{
    public class RoleMetadata
    {
        [JsonIgnore]
        public virtual ICollection<Subscriber> Subscribers { get; set; }
    }

    public class TopicMetaData
    {
        [JsonIgnore]
        public virtual ICollection<Message> Messages { get; set; }

        [JsonIgnore]
        public virtual ICollection<Subscriber> Subscribers { get; set; }
    }

    public class UserMetaData
    {
        [JsonIgnore]
        public virtual ICollection<Message> Messages { get; set; }

        [JsonIgnore]
        public virtual ICollection<Subscriber> Subscribers { get; set; }
    }
}
