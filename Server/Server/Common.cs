﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer
{
    class Common
    {
        public enum UserRole
        {
            publisher,
            subscriber
        }

        public enum UserAction
        {
            broadcastmessage,
            newchannel,
            subscribetochannel,
            readmessage
        }

        public enum LoginStatus
        {
            Admin,
            InvalidUser,
            NewUser
        }
    }
}
