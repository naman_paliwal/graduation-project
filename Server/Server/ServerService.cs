﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace IMQServer
{
    class ServerService
    {
        ServerDetails server;
        public Socket listener;
        IPEndPoint serverEndPoint;

        public ServerService()
        {
            this.server = new ServerDetails();
        }

        public void StartServer()
        {   
            this.listener = server.listener;
            this.serverEndPoint = server.localEndPoint;

            listener.Bind(serverEndPoint);
            listener.Listen(10);
        }
    }
}