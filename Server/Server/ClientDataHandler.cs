﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace IMQServer
{
    class ClientDataHandler
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));

        public void SaveData(string message, Socket clientSocket)
        {
            try
            {
                string clientIP = (((IPEndPoint)clientSocket.RemoteEndPoint).Address.MapToIPv4()).ToString();
                string clientPort = ((IPEndPoint)clientSocket.RemoteEndPoint).Port.ToString();

                string clientFileName = clientIP + "_" + clientPort + ".txt";

                FileStream clientSpecificFile = new FileStream(clientFileName, FileMode.Append, FileAccess.Write);
                StreamWriter streamWriter = new StreamWriter(clientSpecificFile);
                System.DateTime timeStamp = System.DateTime.Now;
                streamWriter.WriteLine(message + "\n" + timeStamp);
                streamWriter.Flush();
                streamWriter.Close();
                clientSpecificFile.Close();
            }
            catch(Exception exception)
            {
                Console.WriteLine("Error in saving the message to file system");
                log.Error(exception);
            }
        }
    }
}
