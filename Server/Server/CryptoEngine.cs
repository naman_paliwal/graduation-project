﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows;

namespace IMQServer
{
    public class CryptoEngine
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));

        public string EncryptString(string plainText)
        {

            byte[] iv = new byte[16];
            byte[] array;
            string sceretKey;

            try
            {
                sceretKey = ConfigurationManager.AppSettings.Get("sceretKey");
                using (Aes aes = Aes.Create())
                {
                    aes.Padding = PaddingMode.Zeros;
                    aes.Key = Encoding.UTF8.GetBytes(sceretKey);
                    aes.IV = iv;

                    ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                        {
                            using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                            {
                                streamWriter.Write(plainText);
                            }

                            array = memoryStream.ToArray();
                        }
                    }
                }

                return Convert.ToBase64String(array);
            }
            catch (Exception exception)
            {
                log.Error("this is error" + exception.Message.ToString(), exception);
                Console.WriteLine("");
            }
            return null;
        }

        public string DecryptString(string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            string sceretKey = ConfigurationManager.AppSettings.Get("sceretKey");
            //log4net.Config.BasicConfigurator.Configure();

            try
            {
                using (Aes aes = Aes.Create())
                {
                    aes.Padding = PaddingMode.Zeros;
                    aes.Key = Encoding.UTF8.GetBytes(sceretKey);
                    aes.IV = iv;
                    ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                    using (MemoryStream memoryStream = new MemoryStream(buffer))
                    {
                        using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                            {
                                return streamReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                log.Error("this is error" + exception.Message.ToString(), exception);
                Console.WriteLine("");
            }
            return null;
        }

    }
}
