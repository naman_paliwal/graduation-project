﻿using System;
using System.Net;
using System.Net.Sockets;

namespace IMQServer
{
    public class ServerDetails
    {
        private IPHostEntry ipHostInfo;
        private IPAddress ipAddress;
        public IPEndPoint localEndPoint { get; set;}
        public Socket listener { get; set;}

        public ServerDetails()
        {
            this.ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            this.ipAddress = ipHostInfo.AddressList[0];
            this.localEndPoint = new IPEndPoint(ipAddress, 11000);
            this.listener = new Socket(ipAddress.AddressFamily,SocketType.Stream, ProtocolType.Tcp);
        }
    }
}