﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient
{
    public enum UserRole
    {
        publisher,
        subscriber
    }

    public enum Action
    {
        broadcastmessage,
        newchannel,
        subscribetochannel,
        readmessage
    }

    public enum LoginStatus
    {
        Admin,
        InvalidUser,
        NewUser
    }
}
