﻿using CommandLine;
using System;
using System.Linq;
using System.Reflection;

namespace IMQClient
{
    class CommandLineParser
    {
        public Type[] LoadVerbs()
        {
            return Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.GetCustomAttribute<VerbAttribute>() != null).ToArray();
        }
    }



    [Verb("publish", HelpText = "This is to publish messages")]
    internal class PublishOptions
    {
        [Option('t', "topic", Required = true)]
        public string TopicName { get; set; }
        [Option('m', "message", Required = true)]
        public string Message { get; set; }
    }

    [Verb("read", HelpText = "This is to publish messages")]
    internal class ReadOptions
    {
        [Option('t', "topic", Required = true)]
        public string TopicName { get; set; }
    }

    [Verb("subscribe", HelpText = "This is to publish messages")]
    internal class SubscribeOptions
    {
        [Option('t', "topic", Required = true)]
        public string TopicName { get; set; }
    }

    [Verb("create", HelpText = "This is to publish messages")]
    internal class CreateTopicOptions
    {
        [Option('t', "topic", Required = true)]
        public string TopicName { get; set; }
    }
}
