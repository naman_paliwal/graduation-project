﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace IMQClient
{
    class Program
    {
        log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
        
        static void Main(string[] args)
        {
            ClientService clientService = new ClientService();
            clientService.StartClient();
            Console.WriteLine("Thanks for connecting with us. Press any key to exit");
            Console.ReadKey();
        }

        
    }
}
