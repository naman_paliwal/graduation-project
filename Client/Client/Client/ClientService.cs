﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using IMQClient.DomainClass;
using Newtonsoft.Json;

namespace IMQClient
{
    class ClientService
    {
        ClientDetails clientDetails;
        Socket client;
        byte[] bytes = new byte[102400];
        IMQRequest imqRequest;
        IMQResponse imqResponse;
        CryptoEngine cryptoEngine;
        List<Topic> topicList;
        List<int> channelsId;
        bool IsAdmin = false;
        CommandLineParser commandLineParser;
        List<Message> mesList;
        int userId;

        public ClientService()
        {
            this.clientDetails = new ClientDetails();
            this.client = this.clientDetails.client;
            this.cryptoEngine = new CryptoEngine();
            this.imqRequest = new IMQRequest();
            this.imqResponse = new IMQResponse();
            this.channelsId = new List<int>();
            this.commandLineParser = new CommandLineParser();
        }

        public void StartClient()
        {
            try 
            {
                this.client.Connect(this.clientDetails.serverEP);

                Console.Write("Please enter your name : ");
                String clientName = Console.ReadLine();
                Console.Write("Please enter your id (or press enter if login in for first time) : ");
                String clientId = Console.ReadLine();
                Console.WriteLine();

                imqRequest.action = "login";
                imqRequest.message = clientName;
                imqRequest.userId = clientId != "" ? Int32.Parse(clientId) : -1;
                this.SendMessage();
                int bytesRec = this.client.Receive(bytes);
                this.imqResponse = JsonConvert.DeserializeObject<IMQResponse>(Encoding.ASCII.GetString(bytes, 0, bytesRec).ToString());

                if(imqResponse.Status == LoginStatus.InvalidUser.ToString())
                {
                    Console.WriteLine("Invalid Username and Password");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else if (imqResponse.Status == LoginStatus.NewUser.ToString())
                {
                    Console.WriteLine("Please note down Id for login in future");
                    Console.Write("Your Id : " + imqResponse.userId);
                }
                else if (imqResponse.Status == LoginStatus.Admin.ToString())
                {
                    this.IsAdmin = true;
                }
                this.userId = imqResponse.userId;

                this.topicList = JsonConvert.DeserializeObject<List<Topic>>(imqResponse.Data,new JsonSerializerSettings
                                    {
                                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                    });
                this.PrintTopicList();
                this.topicList.ForEach(t => this.channelsId.Add(t.TopicId));
                List<string> argsParameter;
                while (true)
                {
                    //Console.WriteLine("Press 1 to create new channel");
                    //Console.WriteLine("Press 2 to sub to new channel");
                    //Console.WriteLine("Press 3 to publish message to channel");
                    //Console.WriteLine("Press 4 to read new message");
                    Console.Write("IMQ>");
                    var userAction = Console.ReadLine();
                    if (userAction.Contains("-m"))
                    {
                        string message = userAction.Split(new string[] { "-m " }, StringSplitOptions.None)[1];
                        // var argsParameter;
                        argsParameter = userAction.Split(new string[] { " -m" }, StringSplitOptions.None)[0].Split().ToList();
                        argsParameter.Add("-m");
                        argsParameter.Add(message);
                    }
                    else
                    {
                        argsParameter = userAction.Split().ToList();
                    }
                    var types = this.commandLineParser.LoadVerbs();
                    
                    Parser.Default.ParseArguments(argsParameter.ToArray(), types)
                        .WithParsed(Run)
                        .WithNotParsed(HandleErrors);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                Console.WriteLine("System is facing some technical issue. Please connect later");
            } 
        }

        private void Run(object obj)
        {
            switch (obj)
            {
                case PublishOptions p:
                    if (this.IsAdmin)
                    {
                        this.PublishMessage(p);
                    }
                    else
                    {
                        Console.WriteLine("You are not authorize person for this action");
                    }
                    break;
                case ReadOptions r:
                    this.ReadMessages(r);
                    break;
                case SubscribeOptions s:
                    this.SubscribeToNewTopic(s);
                    break;
                case CreateTopicOptions c:
                    if (this.IsAdmin)
                    {
                        this.CreateNewTopic(c);
                    }
                    else
                    {
                        Console.WriteLine("You are not authorize person for this action");
                    }
                    break;
            }
        }

        private static void HandleErrors(IEnumerable<Error> obj)
        {
        }

        void SendMessage()
        {
            string enrtyptedMessage = this.cryptoEngine.EncryptString(JsonConvert.SerializeObject(imqRequest).ToString());

            byte[] byteMsg = Encoding.ASCII.GetBytes(enrtyptedMessage);
            int bytesSent = this.client.Send(byteMsg);            
        }

        void GetAcknowlegment()
        {
            int bytesRec = this.client.Receive(bytes);
            string decryptedMessage = this.cryptoEngine.DecryptString(Encoding.ASCII.GetString(bytes, 0, bytesRec).ToString());
            this.imqResponse = JsonConvert.DeserializeObject<IMQResponse>(decryptedMessage);
            if(this.imqResponse.Data != null)
            {
                this.mesList = JsonConvert.DeserializeObject<List<Message>>(this.imqResponse.Data, new JsonSerializerSettings
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                });
            }
            // Console.WriteLine("Message from server = {0}", this.imqResponse.message);
            // throw new ArgumentNullException();
            Console.WriteLine();
        }

        void PublishMessage(PublishOptions publishOptions)
        {
            Topic topic = this.topicList.Where(x => x.TopicName == publishOptions.TopicName).FirstOrDefault();
            // && x.Role == "publisher"
            if(topic == null)
            {
                Console.WriteLine("Wrong Topic or Not authorise");
                return;
            }
            this.imqRequest.message = publishOptions.Message;
            this.imqRequest.channelId = topic.TopicId;
            this.imqRequest.action = Action.broadcastmessage.ToString();
            this.SendMessage();
            this.GetAcknowlegment();
        }

        void CreateNewTopic(CreateTopicOptions createTopicOptions)
        {   
            this.imqRequest = new IMQRequest();
            imqRequest.action = Action.newchannel.ToString();

            this.imqRequest.info = createTopicOptions.TopicName;

            this.SendMessage();
            this.GetAcknowlegment();
        }

        void SubscribeToNewTopic(SubscribeOptions subscribeOptions)
        {
            Topic topic = this.topicList.Where(x => x.TopicName == subscribeOptions.TopicName).FirstOrDefault();
            if (topic == null)
            {
                Console.WriteLine("Topic does not exist");
                return;
            }
            else if (topic.Role == UserRole.subscriber.ToString() || topic.Role == UserRole.publisher.ToString())
            {
                Console.WriteLine("Already a subscriber or publisher");
                return;
            }
            
            this.imqRequest = new IMQRequest();
            this.imqRequest.action = Action.subscribetochannel.ToString();
            this.imqRequest.channelId = topic.TopicId;
            this.SendMessage();
            this.GetAcknowlegment();
            if(this.imqResponse.Status == "200")
            {
                this.topicList.Find(x => x.TopicId == topic.TopicId).Role = UserRole.subscriber.ToString();
            }
        }

        void ReadMessages(ReadOptions readOptions)
        {
            Topic topic = this.topicList.Where(x => x.TopicName == readOptions.TopicName && x.Role == UserRole.subscriber.ToString()).FirstOrDefault();
            if(topic == null)
            {
                Console.WriteLine("Wrong Topic or Not authorise");
                return;
            }
            int id = topic.TopicId;
            this.imqRequest = new IMQRequest() { channelId = id, action = Action.readmessage.ToString(), userId = this.userId};

            this.SendMessage();
            this.GetAcknowlegment();
            this.PrintMessages();
        }

        void PrintTopicList()
        {
            foreach(var topic in this.topicList)
            {
                Console.Write(topic.TopicId + "   " + topic.TopicName + "   ");
                Console.WriteLine(topic.Role == UserRole.publisher.ToString() ? "subscribed" : topic.Role == UserRole.subscriber.ToString() ? UserRole.subscriber.ToString() : "Not subscribed");
            }
        }

        void PrintMessages()
        {
            foreach(Message message in this.mesList.AsEnumerable().Reverse())
            {
                Console.WriteLine("Time" + message.date);
                Console.WriteLine("Message : " + message.ClientMessage);
                Console.WriteLine();
            }
        }
    }
}
