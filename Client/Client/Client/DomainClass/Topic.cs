﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient.DomainClass
{
    class Topic
    {
        public int TopicId { get; set; }
        public string TopicName { get; set; }
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}
