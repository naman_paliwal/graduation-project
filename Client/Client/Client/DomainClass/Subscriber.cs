﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient.DomainClass
{
    class Subscriber
    {
        public int Id { get; set; }
        public Nullable<int> role_RoleId { get; set; }
        public Nullable<int> topic_Id { get; set; }
        public Nullable<int> user_Id { get; set; }

        public virtual Role Role { get; set; }
        public virtual Topic Topic { get; set; }
        public virtual User User { get; set; }
    }
}
