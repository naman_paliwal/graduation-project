﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient.DomainClass
{
    class Message
    {
        public int Id { get; set; }
        public string ClientMessage { get; set; }
        public System.DateTime date { get; set; }
        public bool IsDead { get; set; }
        public Nullable<int> topic_Id { get; set; }
        public Nullable<int> user_Id { get; set; }

        public virtual Topic Topic { get; set; }
        public virtual User User { get; set; }
    }
}
