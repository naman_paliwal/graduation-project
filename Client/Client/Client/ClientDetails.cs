﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient
{
    class ClientDetails
    {
        public IPEndPoint serverEP;
        public Socket client;

        public ClientDetails()
        {
            IPAddress ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
            this.serverEP = new IPEndPoint(ipAddress,11000);
            this.client = new Socket(ipAddress.AddressFamily,SocketType.Stream, ProtocolType.Tcp);
        }            
    }
}
