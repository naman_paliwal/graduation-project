﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IMQClient
{
    public class IMQProtocol
    {
        public IMQProtocol()
        {
            this.dataFormat = "JSON";
        }
        public string dataFormat;
        public string action;
        public int channelId;
        public string info;
        public string message;
        public int userId;
    }
}
