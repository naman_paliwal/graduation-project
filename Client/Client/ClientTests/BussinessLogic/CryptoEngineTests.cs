﻿using IMQClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ClientTests.BussinessLogic
{
    public class CryptoEngineTests
    {
        [Fact]
        public void EncryptStringTest()
        {
            string message = "This is test message.";
            string actualEncryptedMessage = "TO6IX7ZLh3jRI1s3aqCm7fyY588bMyxuDveWT9PTvfaI3PvvPYeooVYV7EeKCRE+nuCSYhWNBZAP8uBLt1k2pRV2fTSq8H7bhRXQqiyW7DQ=";
            CryptoEngine cryptoEngine = new CryptoEngine();

            string encryptedMessage = cryptoEngine.EncryptString(message);

            Assert.Equal(actualEncryptedMessage, encryptedMessage);
        }

        [Fact]
        public void DecryptStringTest()
        {
            string message = "This is test message.";
            string encryptedMessage = "TO6IX7ZLh3jRI1s3aqCm7fyY588bMyxuDveWT9PTvfaI3PvvPYeooVYV7EeKCRE+nuCSYhWNBZAP8uBLt1k2pRV2fTSq8H7bhRXQqiyW7DQ=";
            CryptoEngine cryptoEngine = new CryptoEngine();

            string decryptedMessage = cryptoEngine.DecryptString(encryptedMessage);

            Assert.Equal(decryptedMessage, message);
        }
    }
}
